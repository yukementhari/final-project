<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Berita;
use App\Liked;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $home = Berita::all();
        return view('home.index',compact('home'));
    }

    public function show($id)
    {
        $home = Berita::find($id)->first();
        return view('home.show', compact('home'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'berita_id' => 'required',
            'nama' => 'required',
            'email' => 'required',
            'reaksi' => 'required',
            'komentar' => 'required'
        ]);
        dd($request->all());
        $liked = Liked::create([
            "berita_id" => $request->berita_id,
            "nama" => $request->nama,
            "email" => $request->email,
            "reaksi" => $request->reaksi,
            "komentar" => $request->komentar
        ]);

        return response()->json(['data' => $liked]);
    }
}
