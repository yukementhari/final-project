@extends('home.master1')
@section('content')
    <div class="banner-area banner-inner-1 bg-black" id="banner">
        <div class="container" style="margin: 0 auto;">
            <div class="row justify-content-center">
                <div class="col-lg-8 col-sm-12">
                    <div class="card card-berita">
                        <div class="card-header">{{'Home/'.$home->kategori->nama}}</div>
                        <div class="card-body">
                            <div class="row justify-content-center">
                                <div class="col-md-10 col-12">
                                    <h3>{{$home->judul}}</h3>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-md-10 col-12">
                                    <i class="fa fa-clock-o"></i><span class="ml-2">{{$home->updated_at->toDayDateTimeString()}}</span>
                                </div>
                            </div>
                            <div class="row justify-content-center py-2">
                                <div class="col-md-8 col-12">
                                    <img src="{{asset('poster/'.$home->poster)}}" alt="Gambar {{$home->judul}}">
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-8">
                                    <p class="pt-4">Penulis : {{$home->penulis}}</p>
                                </div>
                                <div class="col-md-8 col-12">
                                    <p class="">{{$home->deskripsi}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row justify-content-center">
                                <div class="col-md-8 col-12">
                                    <div class="comment-title">
                                        <h5>Comment</h5>
                                    </div>
                                    <div class="comment-post">
                                        <div class="comment-body">
                                            <div class="comment-heading">
                                                <h6>Arya</h6>
                                                <span class="">September 13, 2021</span>
                                            </div>
                                            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Doloremque voluptatem dolorum natus impedit vero autem nulla sed dolores suscipit quas?</p>
                                            <div class="comment-footer">
                                                <a href="#">Reply</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-md-8 col-12">
                                    <div class="comment-title">
                                        <h5>Leave your comment</h5>
                                    </div>
                                    <form action="/home/{{$home->id}}" method="post">
                                        @csrf
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-6">
                                                    <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama">
                                                    @error('nama')
                                                    <div class="alert alert-danger">
                                                        {{ $message }}
                                                    </div>
                                                    @enderror
                                                </div>
                                                <div class="col-6">
                                                    <input type="email" class="form-control" name="email" id="email" placeholder="Email">
                                                    @error('email')
                                                    <div class="alert alert-danger">
                                                        {{ $message }}
                                                    </div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>  
                                        <div class="form-group">
                                            <label for="reaksi">Reaksi</label>
                                            <select class="form-control" name="reaksi" id="reaksi">
                                                <option value="">--Pilih Reaksi--</option>
                                                    <option value="Suka">Suka</option>
                                                    <option value="Tidak Suka">Tidak Suka</option>
                                            </select>
                                            @error('reaksi')
                                                <div class="alert alert-danger">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>                                       
                                    
                                        <div class="form-group">
                                            <textarea class="form-control" name="komentar" id="komentar" cols="30" rows="10" placeholder="Masukkan komentar"></textarea>
                                            @error('komentar')
                                            <div class="alert alert-danger">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                        </div>
                                        <button class="btn btn-success">Kirim</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection